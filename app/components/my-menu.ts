import Component from '@glimmer/component';

export default class MyMenuComponent extends Component {
  get destinationElement() {
    return document.getElementById('in-element-container');
  }
}
