# ember-popperjs-bugreport

## Installation

* `git clone <repository-url>` this repository
* `cd ember-popperjs-bugreport`
* `pnpm install`

## Running / Development

* `pnpm start`
* Visit your app at [http://localhost:4200](http://localhost:4200).

## Subject

- Having single (http://localhost:4200/single) ember-popperjs seem to be working nicely.
- When there are multiple (http://localhost:4200/multiple) items, it fails with:
```
Error occurred:

- While rendering:
  -top-level
    application
      multiple
        MyMenuComponent
          Menu
            PopperJS
              Items

Uncaught (in promise) DOMException: Node.removeChild: The node to be removed is not a child of this node
```
- I used the ember-headlessui as shown in [the documentation](https://github.com/NullVoxPopuli/ember-popperjs)

![Screenshot of failing example](doc/screen.png)
