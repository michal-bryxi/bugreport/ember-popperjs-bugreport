import { module, test } from 'qunit';
import { setupTest } from 'ember-popperjs-bugreport/tests/helpers';

module('Unit | Route | single', function (hooks) {
  setupTest(hooks);

  test('it exists', function (assert) {
    const route = this.owner.lookup('route:single');
    assert.ok(route);
  });
});
